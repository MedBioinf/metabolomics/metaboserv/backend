FROM debian:11.8
ENV DEBIAN_FRONTEND=noninteractive

RUN     apt-get update
RUN     apt-get install -y --no-install-recommends build-essential r-base libcurl4 openssl libmagic1 software-properties-common

RUN     Rscript -e "install.packages('mrbin')"
RUN	    apt-get install -y --no-install-recommends python3 python3-pip python3-dev wget libmariadb3 libmariadb-dev


WORKDIR /app
COPY    ./requirements.txt /app/requirements.txt
RUN     pip install -r /app/requirements.txt
RUN	    apt-get install -y iputils-ping

VOLUME  /app/files /app/data /app/uploads
RUN     mkdir /app/elasticsearch

COPY    ./metaboserv_conf_docker.yml /app/metaboserv_conf_docker.yml
COPY    ./src /app/src

ENV     PYTHONPATH=/app

ENTRYPOINT ["python3", "/app/src/metaboserv_api.py", "--docker"]
