import pytest, logging
from metaboserv_database import MetaboservDatabase
from metaboserv_es_data import INDICES, FORBIDDEN_IDS, MAPPINGS
from metaboserv_input import read_config
from metaboserv_permissions import dict2obj

test_logger = logging.getLogger(__name__)
test_logger.setLevel(logging.DEBUG)

def pytest_configure():
    pytest.test_user = None
    pytest.test_user2 = None

@pytest.fixture(scope='module')
def test_db():
    # API to test is assumed to run natively (as this is mostly relevant for development)
    test_logger.debug("Creating DB for testing")
    config = read_config('metaboserv_conf.yml')

    elasticsearch_config = config['elasticsearch']
    mariadb_config = config['mariadb']

    elasticsearch_host = f"http://{elasticsearch_config['path']}:{elasticsearch_config['port']}"
    mariadb_host = f"{mariadb_config['path']}:{mariadb_config['port']}"

    test_database = MetaboservDatabase(elasticsearch_config, elasticsearch_host, mariadb_config,
        mariadb_host, False, config['backend']['admin_password'], config['backend']['source_location'], config['backend']['upload_folder'], config['smtp'])
    admin_account = dict2obj(test_database.user_internal_get_information('admin').getBody())
    yield test_database, admin_account

def test_study_creation(test_db):
    """
    GIVEN a MetaboservDatabase, Elasticsearch and MariaDB instances
    WHEN a new study is created
    THEN check if study can be created
    """
    database, admin_acc = test_db
    assert database.user_check_exists('admin') is True
    # If the test_study already exists, it is deleted
    if database.study_check_exists('test_study'):
        test_logger.debug("Test study already exists from previous tests, deleting...")
        delete = database.study_delete('test_study', admin_acc)
        test_logger.debug(delete.msg)
        test_logger.debug("Test study deleted successfully.")
    test_logger.debug(database.study_check_exists('test_study'))
    assert database.study_check_exists('test_study') is False
    test_logger.debug("Creating test study...")
    database.study_create(
        study_id='test_study',
        study_type='NMR',
        study_specimen='serum',
        study_name='TestStudy123',
        study_date='2012',
        concentration_data={'test1': {'_arginine': '0.01', '_glucose': '0.1', '_ethanol': '1.00'}},
        compounds=["_arginine", "_glucose", "_ethanol"],
        concentration_metadata={},
        author='Tim Tucholski',
        visibility='private',
        metadata={
            'somerandomstuff': 'yes',
        },
        phenotypes=None,
        units=None,
        auth_user=admin_acc
    )
    test_logger.debug("Test study was created successfully.")
    assert database.study_check_exists('test_study') is True
    study_get = database.study_get('test_study', auth_user=admin_acc).getBody()
    # Test whether the created study has the correct parameters
    test_logger.debug("Validating test study parameters...")
    assert study_get['study_id'] == 'test_study'
    assert study_get['study_type'] == 'NMR'
    assert study_get['study_specimen'] == 'serum'
    assert study_get['study_name'] == 'TestStudy123'
    assert study_get['study_author'] == 'Tim Tucholski'
    assert study_get['study_visibility'] == 'private'
    assert study_get['study_metadata']['somerandomstuff'] == 'yes'
    assert admin_acc.username in study_get['study_members'].keys()
    assert study_get['study_members'][admin_acc.username] == 'uploader'
    test_logger.debug("Finished validation of test study parameters")

def test_user_creation(test_db):
    """
    GIVEN a MetaboservDatabase, Elasticsearch instance
    WHEN a new user is created
    THEN check if the creation process occurred without problems
    """
    database, admin_acc = test_db
    # Create user account for testing (also used for subsequent tests)
    if database.user_check_exists('test_user'):
        test_logger.debug("Test user already exists from previous tests, deleting...")
        database.user_delete('test_user', auth_user=admin_acc)
        test_logger.debug("Test user deleted successfully.")
    if database.user_check_exists('test_user2'):
        test_logger.debug("Test user 2 already exists from previous tests, deleting...")
        database.user_delete('test_user2 ', auth_user=admin_acc)
        test_logger.debug("Test user 2 deleted successfully.")
    database.user_create('test_user', 'testpassword123', 'testuser@testmail.com', "University of Testhausen")
    database.user_create('test_user2', 'testpassword321', 'testuser2@testmail.com', "University of Testhausen")
    test_logger.debug("Test users created successfully.")
    assert database.user_check_exists('test_user') is True
    assert database.user_check_exists('test_user2') is True
    test_logger.debug("Validating user parameters..")
    # Validate parameters
    pytest.test_user = dict2obj(database.user_authenticate_and_return('test_user', 'testpassword123'))
    pytest.test_user2 = dict2obj(database.user_authenticate_and_return('test_user2', 'testpassword321'))
    user_get = database.user_get_information('test_user', auth_user=admin_acc).getBody()
    assert user_get is not None
    assert user_get['role'] == "user"
    assert user_get['password'] != 'testpassword123'
    assert user_get['salt'] is not None
    assert user_get['username'] == "test_user"
    assert user_get['email'] == "testuser@testmail.com"
    assert user_get['institution'] == "University of Testhausen"
    test_logger.debug("Finished validation of test user parameters.")

def test_unauthorized_access(test_db):
    """
    GIVEN a MetaboservDatabase, Elasticsearch instance, MariaDB instance
    WHEN an unauthorized user tries actions that are not permitted
    THEN check whether access is correctly denied
    """
    database, admin_acc = test_db
    # Check whether user can access a private study
    test_logger.debug("Starting auth/auth validation..")
    study_get = database.study_get('test_study', auth_user=pytest.test_user)
    assert study_get.status == 401
    # Check whether any study incorrectly appears in their private list
    study_list_private = database.study_list_private(auth_user=pytest.test_user).getBody()
    assert study_list_private == {}
    # Check whether a contributor can change the uploader's status
    database.user_make_contributor('test_user', 'test_study', auth_user=admin_acc)
    change_uploader = database.user_make_contributor('admin', 'test_study', auth_user=pytest.test_user)
    study_get = database.study_get('test_study', auth_user=pytest.test_user)
    assert change_uploader.status == 200 and study_get.status == 200
    assert study_get.getBody()['study_members']['admin'] == "uploader"
    database.user_revoke_permissions('test_user', 'test_study', auth_user=admin_acc)
    assert database.user_check_authorization(pytest.test_user, 'test_study', required_permission="viewer") is False
    # Check whether a viewer can not add new contributors or change metadata
    database.user_make_viewer('test_user', 'test_study', auth_user=admin_acc)
    study_update = database.study_update_metadata('test_study', {'somekey': 'somevalue'}, auth_user=pytest.test_user)
    assert study_update.status == 401
    make_viewer = database.user_make_viewer('test_user2', 'test_study', auth_user=pytest.test_user)
    make_contributor = database.user_make_contributor('test_user2', 'test_study', auth_user=pytest.test_user)
    assert make_viewer.status == 401 and make_contributor.status == 401
    # Check whether user can incorrectly delete a study
    delete = database.study_delete('test_study', auth_user=pytest.test_user)
    assert delete.status == 401

def test_authorized_access(test_db):
    """
    GIVEN a MetaboservDatabase, Elasticsearch instance, MariaDB instance
    WHEN a user has the necessary permissions to perform an action
    THEN check whether the user can actually perform said action
    """
    database, admin_acc = test_db
    # Check whether user can access a private study
    study_get = database.study_get('test_study', auth_user=pytest.test_user)
    assert study_get.status == 200
    # Check whether any study correctly appears in their private list
    study_list_private = database.study_list_private(auth_user=pytest.test_user).getBody()
    assert 'test_study' in study_list_private.keys()
    # Check whether a contributor correctly can change metadata
    database.user_make_contributor('test_user', 'test_study', auth_user=admin_acc)
    study_update = database.study_update_metadata('test_study', {'somekey': 'somevalue'}, auth_user=pytest.test_user)
    assert study_update.status == 200
    study_get = database.study_get('test_study', auth_user=pytest.test_user).getBody()
    assert 'somekey' in study_get['study_metadata'].keys()
    assert study_get['study_metadata']['somekey'] == 'somevalue'
    # Check whether a contributor can add new viewers/contributors
    make_viewer = database.user_make_viewer('test_user2', 'test_study', auth_user=pytest.test_user)
    study_get = database.study_get('test_study', auth_user=pytest.test_user)
    assert make_viewer.status == 200 and study_get.status == 200
    assert study_get.getBody()['study_members']['test_user2'] == "viewer"
    make_contributor = database.user_make_contributor('test_user2', 'test_study', auth_user=pytest.test_user)
    study_get = database.study_get('test_study', auth_user=pytest.test_user)
    assert make_contributor.status == 200 and study_get.status == 200
    assert study_get.getBody()['study_members']['test_user2'] == "contributor"


    

