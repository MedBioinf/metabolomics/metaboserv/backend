import json

class AuthenticationObject:

    _username: str
    _auth_token: str
    _admin_permissions: bool

    def __init__(self, user, token):
        self._username = user
        self._auth_token = token

class obj(object):
    def __init__(self, dict_):
        self.__dict__.update(dict_)

def dict2obj(d):
    return json.loads(json.dumps(d), object_hook=obj)
