import json
import logging, io, matplotlib
import re
import os
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
import requests
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.patches import Rectangle
from collections import defaultdict, OrderedDict
from metaboserv_input import ResultObject
from secrets import token_urlsafe
matplotlib.use('Agg')

class MetaboservPlotter:

    plot_map: OrderedDict

    def __init__(self):
        self.plot_map = {}
        plt.rc('ytick', labelsize=12)
        plt.rc('xtick', labelsize=12)

    def save_plot(self, plot_image: io.BytesIO, plot_id:str=None) -> ResultObject:
        """
        Saves a plot in the plot map.
        It can then be retrieved using the plot id.
        :param plot_image - The plot image to save
        :param plot_id - ID of the plot. If not supplied, a random one is generated.
        """
        if len(self.plot_map.keys()) > 50:
            self.plot_map.popitem(last=False)
        if not plot_id: plot_id = token_urlsafe(16)
        self.plot_map[plot_id] = plot_image
        return ResultObject(body={'plot_id': plot_id})

    def retrieve_plot(self, plot_id: str) -> ResultObject:
        """
        Retrieves a plot by its ID.
        :param plot_id - ID of the plot
        """
        if plot_id not in self.plot_map.keys(): return ResultObject(msg=f"Plot {plot_id} was not found!", status=404)
        else: return ResultObject(body=self.plot_map[plot_id])

    def query_results_to_dataframe(self, query_results: dict, aggregate: bool = True) -> ResultObject:
        """
        Turns query results (e.g. from the MetaboservQuery endpoint) into a pandas dataframe for easier plotting.
        Original result format is {study: {source: {compound/phenotype: value}}}
        Resulting dataframe rows contain study, compounds and phenotypes.
        :param query_results - The query results to convert
        :param aggregate - Indicates whether to aggregate results by source ID
        """

        def return_first_value(x: list) -> float:
            non_nan = [val for val in x if not pd.isna(val)]
            return non_nan[0] if non_nan else np.nan

        try:
            final_df = pd.DataFrame
            for study_id in query_results.keys():
                res_dict = {}
                for source_id in query_results[study_id].keys():
                    res_dict[source_id] = {'study_id': study_id}
                    for key in query_results[study_id][source_id].keys():
                        potential_value = query_results[study_id][source_id][key]
                        res_dict[source_id][key] = potential_value if potential_value else np.nan
                if final_df.empty:
                    final_df = pd.DataFrame.from_dict(res_dict, orient='index')
                else:
                    final_df = pd.concat([final_df, pd.DataFrame.from_dict(res_dict, orient='index')], ignore_index=False)
            if aggregate:
                agg_df = final_df.groupby(final_df.index).agg({
                    lambda s: ",".join(s.astype(str).dropna()) if s.name == "study_id" else (return_first_value(s))
                })
                agg_df.columns = final_df.columns
                print(agg_df.iloc[0:10])
            return ResultObject(body=agg_df if aggregate else final_df)
        except Exception as e:
            logging.error(f"Encountered an error while converting query results to pandas dataframe: {e}")
            return ResultObject(msg=f"Encountered an error while converting query results to pandas dataframe: {e}", status=500)
        
    def generate_scatterplot(self, query_frame: pd.DataFrame, metabolites_to_plot:list=None, vis_ranges:dict=None, phenotype_to_plot:str=None, units:dict=None, plot_id:str=None) -> ResultObject:
        """
        Generates a scatterplot depicting the concentration levels of two metabolites.
        :param query_frame - Data to plot in the format of a pandas DataFrame
        :param metabolites_to_plot - The two metabolites to plot
        :param vis_ranges - Visualization ranges to use in the plot (optional)
        :param phenotype_to_plot - Phenotype to use in the scatterplot (optional)
        :param units - Units to plot (optional, will be "Unknown unit" otherwise)
        :param plot_id - If supplied, plot is generated with this particular ID (otherwise random)
        """
        try:
            if len(metabolites_to_plot) == 1:
                return ResultObject(msg="No scatterplot drawn as only metabolite was provided.", status=200)
            if not metabolites_to_plot or not isinstance(metabolites_to_plot, list) or not len(metabolites_to_plot) == 2:
                return ResultObject(msg="Plot creation failed, exactly two metabolites are required for a scatterplot!", status=400)
            f, ax = plt.subplots(figsize=(11, 9))

            study_number = query_frame['study_id'].nunique()
            c_palette = "Blues" if study_number == 1 else sns.diverging_palette(250, 10, n=study_number)
            if phenotype_to_plot:
                scatterplot = sns.scatterplot(data=query_frame, x=metabolites_to_plot[0], y=metabolites_to_plot[1], hue="study_id",  style=phenotype_to_plot, palette=c_palette, s=100)
            else:
                scatterplot = sns.scatterplot(data=query_frame, x=metabolites_to_plot[0], y=metabolites_to_plot[1], hue="study_id", palette=c_palette, s=100)


            if vis_ranges and metabolites_to_plot[0] in vis_ranges.keys():
                mx_ranges = vis_ranges[metabolites_to_plot[0]]
                if 'max' not in mx_ranges.keys():
                    mx_ranges['max'] = query_frame[metabolites_to_plot[0]].max()
                ax.add_patch(Rectangle((float(mx_ranges['min']), ax.get_ylim()[0]), (float(mx_ranges['max'])-float(mx_ranges['min'])), ax.get_ylim()[1]+abs(0-ax.get_ylim()[0]), facecolor="lightgreen", fill="true", zorder=0, alpha=0.1))
            if vis_ranges and metabolites_to_plot[1] in vis_ranges.keys():
                my_ranges = vis_ranges[metabolites_to_plot[1]]
                if 'max' not in my_ranges.keys():
                    my_ranges['max'] = query_frame[metabolites_to_plot[1]].max()
                ax.add_patch(Rectangle((ax.get_xlim()[0], float(my_ranges['min'])), ax.get_xlim()[1]+abs(0-ax.get_xlim()[0]), (float(my_ranges['max'])-float(my_ranges['min'])), facecolor="lightgreen", fill="true", zorder=0, alpha=0.1))

            legend = plt.legend()
            for i, text in enumerate(legend.get_texts()):
                    if str(text.get_text()).startswith('PHENOTYPE_'):
                        legend.get_texts()[i].set_text(str(text.get_text()).replace('PHENOTYPE_', 'Phenotype: '))
                    if str(text.get_text()) == 'study_id':
                        legend.get_texts()[i].set_text('Study')

            if units and metabolites_to_plot[0] in units.keys():
                if (isinstance(units[metabolites_to_plot[0]], list) and len(units[metabolites_to_plot[0]]) != 1):
                    return ResultObject(msg=f"Sorry, there are multiple units for {metabolites_to_plot[0]} - unable to draw this particular study combination!", status=416)
            if units and metabolites_to_plot[1] in units.keys():
                if (isinstance(units[metabolites_to_plot[1]], list) and len(units[metabolites_to_plot[0]]) != 1):
                    return ResultObject(msg=f"Sorry, there are multiple units for {metabolites_to_plot[1]} - unable to draw this particular study combination!", status=416)
                    
            plt.title(f"Sample Distribution for {metabolites_to_plot[0]} / {metabolites_to_plot[1]}", loc='left', fontsize=19)
            unit_to_plot_x = 'Unknown unit' if not (units and metabolites_to_plot[0] in units.keys()) else (units[metabolites_to_plot[0]][0] if isinstance(units[metabolites_to_plot[0]], list) else units[metabolites_to_plot[0]])
            unit_to_plot_y = 'Unknown unit' if not (units and metabolites_to_plot[1] in units.keys()) else (units[metabolites_to_plot[1]][0] if isinstance(units[metabolites_to_plot[1]], list) else units[metabolites_to_plot[1]])

            plt.xlabel(f"{metabolites_to_plot[0]} ({unit_to_plot_x})", fontsize=15)
            plt.ylabel(f"{metabolites_to_plot[1]} ({unit_to_plot_y})", fontsize=15)
            plt.tight_layout()

            fig = scatterplot.get_figure()
            fig.savefig("out_scat.png", dpi=400)

            bytes_image = io.BytesIO()
            FigureCanvas(f).print_png(bytes_image)
            plt.close('all')

            plot_id = self.save_plot(bytes_image, plot_id).getBody()
            return ResultObject(body=plot_id)
        except Exception as e:
            logging.error(f"Error during scatterplot creation: {e}")
            return ResultObject(msg="Sorry, an error occurred during scatterplot creation!", status=500)
        
    def generate_histogram(self, query_frame: pd.DataFrame, metabolite_to_plot: str, vis_ranges:dict=None,
                phenotype_to_plot:str=None, units:dict=None, plot_id:str=None, histstyle:str="step|fill", bins:int=30) -> ResultObject:
        """
        Generates a histogram.
        :param query_frame - Data to plot in the format of a pandas DataFrame
        :param metabolite_to_plot - The metabolite to plot
        :param vis_ranges - Range to visualize (optional)
        :param phenotype_to_plot - Phenotype to plot (optional)
        :param plot_id - The ID the plot is saved as, can be supplied (otherwise it is random)
        :param histstyle - Style of the histogram (step/fill)
        :param bins - Bins in the histogram (30 by default)
        :returns BytesIO object containing the plot image
        """
        try:
            if not metabolite_to_plot or query_frame.empty:
                return ResultObject(msg="Plot creation failed!", status=400)

            if phenotype_to_plot:
                pheno_hue = query_frame[["study_id", phenotype_to_plot]].dropna().apply(tuple, axis=1)

            f, ax = plt.subplots(figsize=(11, 9))

            hist_split = histstyle.split("|")
            hist_element = hist_split[0]
            hist_fill = True if hist_split[1] == "fill" else False
            
            if phenotype_to_plot:
                histplot = sns.histplot(data=query_frame, x=metabolite_to_plot, palette="muted", hue=pheno_hue, bins=max(1,int(bins)),
                    alpha=0.6, zorder=1, stat="probability", common_norm=False, element=hist_element, multiple="layer", fill=hist_fill)
            else:
                histplot = sns.histplot(data=query_frame, x=metabolite_to_plot, palette="Blues", hue="study_id", bins=max(1,int(bins)),
                    alpha=0.6, zorder=1, stat="probability", common_norm=False, element=hist_element, multiple="layer", fill=hist_fill)
                

            if vis_ranges and metabolite_to_plot in vis_ranges.keys():
                mx_range = vis_ranges[metabolite_to_plot]
                if 'max' not in mx_range.keys():
                    mx_range['max'] = query_frame[metabolite_to_plot].max()
                ax.add_patch(Rectangle((float(mx_range['min']), 0), (float(mx_range['max'])- float(mx_range['min'])), ax.get_ylim()[1], facecolor="lightgreen", fill="true", zorder=0, alpha=0.1))


            plt.title(f"Concentration Histogram for {metabolite_to_plot}", loc='left', fontsize=19)
            if units and metabolite_to_plot in units.keys():
                if (isinstance(units[metabolite_to_plot], list) and len(units[metabolite_to_plot]) != 1):
                    return ResultObject(msg="Sorry, there are multiple units for the chosen metabolite - unable to draw this particular study combination!", status=416)
            unit_to_plot = 'Unknown unit' if not (units and metabolite_to_plot in units.keys()) else (units[metabolite_to_plot][0] if isinstance(units[metabolite_to_plot], list) else units[metabolite_to_plot])
            plt.xlabel(f"{metabolite_to_plot} ({unit_to_plot})", fontsize=15)
            plt.ylabel("Percentage (in respective study)", fontsize=15) 
            plt.tight_layout()

            fig = histplot.get_figure()
            fig.savefig("out_hist.png", dpi=400)

            bytes_image = io.BytesIO()
            FigureCanvas(f).print_png(bytes_image)
            plt.close('all')

            plot_id = self.save_plot(bytes_image, plot_id).getBody()
            return ResultObject(body=plot_id)
        except Exception as e:
            logging.error(f"Error during histogram creation: {e}")
            return ResultObject(msg="Sorry, an error occurred during histogram creation!", status=500)
    
    def generate_correlation_plot(self, query_frame: pd.DataFrame, plot_id:str=None) -> ResultObject:
        """
        Generates a heatmap.
        :param query_frame: pandas DataFrame containing the data to plot
        :param plot_id - The ID the plot is saved as, can be supplied (otherwise it is random)
        :returns BytesIO object containing the plot image
        """
        try:
            # Remove study and phenotypes from input data, as they are not required here
            df = query_frame.drop(['study_id'], axis=1)
            df_cleaned = df[df.columns.drop(list(df.filter(regex='PHENOTYPE_')))]

            correlation_map = df_cleaned.astype(float).corr(method='pearson')
            f, _ = plt.subplots(figsize=(min(100, len(df)), min(100, len(df))))
            cmap = sns.diverging_palette(250, 10, as_cmap=True)
            #cmap = sns.color_palette("coolwarm", as_cmap=True)
            mask = np.zeros_like(correlation_map, dtype=bool)
            mask[np.triu_indices_from(mask)] = True
            
            heatmap = sns.heatmap(correlation_map, mask=mask, cmap=cmap, center=0, square=True, linewidths=0.5)
            plt.title("Correlation map for selected samples", loc='left', fontsize=19)
            plt.tight_layout()

            #fig = heatmap.get_figure()
            #fig.savefig("out_hm.png", dpi=400)

            bytes_image = io.BytesIO()
            FigureCanvas(f).print_png(bytes_image)
            plt.close('all')

            plot_id = self.save_plot(bytes_image, plot_id).getBody()
            return ResultObject(body=plot_id)
        except Exception as e:
            logging.error(f"Error during corr. plot creation: {e}")
            return ResultObject(msg="Sorry, an error occurred during corr. plot creation!", status=500)
        
    def shrinkArray(self, array: np.ndarray, binsize: int, cap: int):
        """
        Helper method that bins the spectrum and caps peaks to a certain value (for viewing purposes).

        :param array: The raw spectrum array, containing two dimensions (chemical shift values and peak intensities)
        :param binsize: Number of entries to summarize in one bin
        :param cap: Cutoff value for peaks
        """
        new_array = np.array([])
        for i in range(0, int(np.ceil(array.size/binsize))):
            new_array = np.append(new_array, min(np.average(array[i*binsize : i*binsize+binsize+1]), cap))
        return new_array

        
    def plot_spectrum(self, folder: str, study: str, filename: str, spec_url: str, plot_id: str) -> ResultObject:
        """
        Attempts plotting a raw Bruker spectrum.
        :param folder: The sourcefile folder (where spectra are uploaded).
        :param study: The study to plot for.
        :param filename: The file to plot.
        :param spec_url: The spectra service URL.
        :param plot_id: ID for the plot.
        """
        try:
            if not os.path.isfile(os.path.join(folder, study, filename)):
                return ResultObject(msg="File not found!", status=404)
            spectra_results = requests.get(f"http://{spec_url}/NMR/v1/parseSpectrum_Bruker", params={'study_id': study, 'filename': filename})
            if spectra_results.status_code == 200:
                spr = spectra_results.json()
                ppm_array = np.asarray([float(re.sub("\[|\]", "", x)) for x in spr[0].split(",")])
                mag_array = np.asarray([float(re.sub("\[|\]", "", x)) for x in spr[1].split(",")])

                ppm_array = self.shrinkArray(array=ppm_array, binsize=100, cap=600000)
                mag_array = self.shrinkArray(array=mag_array, binsize=100, cap=600000)

                if ppm_array.size != mag_array.size:
                    return ResultObject(msg="Arrays for ppm values and magnitude differ in size. Aborting.", status=500)
                
                f, _ = plt.subplots(figsize=(14, 11))
                lineplot = plt.plot(ppm_array, mag_array)

                plt.title(f"Bruker NMR plot for {filename}", loc='left', fontsize=19)
                plt.xlabel(f"ppm", fontsize=15)
                plt.ylabel("Intensity (capped at 600000)", fontsize=15) 
                plt.tight_layout()

                bytes_image = io.BytesIO()
                FigureCanvas(f).print_png(bytes_image)
                #fig = lineplot.get_figure()
                #fig.savefig("out_qc.png", dpi=400)

                plt.close('all')

                plot_id = self.save_plot(bytes_image, plot_id).getBody()
                return ResultObject(body=plot_id)
            else:
                return ResultObject(msg="R Spectra Parser has exited with an error code, sorry!", status=500)
            return ResultObject(msg="Worked")
        except Exception as e:
            logging.error(f"Error during raw spectrum plot generation: {e}")
            return ResultObject(msg="Sorry, an error occurred during raw spectrum plot generation!", status=500)
    
    def generate_quality_plot_per_metabolite(self, query_frame: pd.DataFrame, plot_id:str=None, over_lod:bool=True, study_id:str=None, compound_amount:int=None) -> ResultObject:
        """
        Generates a quality control plot which depicts the number of (non-)NA values per metabolite.
        :param query_frame: pandas DataFrame containing the data to plot
        :param plot_id: The ID the plot is saved as, can be supploed (otherwise it is random)
        :param over_lod: If true, a >LOD plot will be drawn, otherwise <LOD.
        :param compound_amount: Number of compounds (can optionally be passed), used for horizontal figure growth
        :returns BytesIO object containing the plot image
        """
        if query_frame.empty or not study_id:
            return ResultObject(msg="Plot creation failed!", status=400)
        
        # Remove study and phenotypes from input data, as they are not required here
        df = query_frame.drop(['study_id'], axis=1)
        df = df[df.columns.drop(list(df.filter(regex='PHENOTYPE_')))]

        na_df = (df.isnull().sum() / len(df)) * 100
        if over_lod:
            missing_data = pd.DataFrame({'value': 100 - na_df})
        else:
            missing_data = pd.DataFrame({'value': na_df})

        f, ax = plt.subplots(figsize=(15, 9) if not compound_amount else (min(400, compound_amount/7), 9))

        _ = plt.xticks(ticks=range(0,len(missing_data)), labels=missing_data.index, rotation=90)
        barplot = sns.barplot(data=missing_data, x=missing_data.index, y='value', hue=missing_data.index, legend=False, dodge=False)
        
        plt.margins(x=0.03, tight=True)
        plt.title(f"Quality Control Plot for study {study_id}", loc='left', fontsize=19)
        plt.xlabel(f"Metabolite", fontsize=15)
        plt.ylabel(f"Number of samples {'>' if over_lod else '<'} LOD (%)", fontsize=15)
        _ = plt.ylim(0, 105)
        plt.tight_layout()

        bytes_image = io.BytesIO()
        FigureCanvas(f).print_png(bytes_image)
        #fig = barplot.get_figure()
        #fig.savefig("out_qc.png", dpi=400)
        plt.close('all')

        plot_id = self.save_plot(bytes_image, plot_id).getBody()
        return ResultObject(body=plot_id)

    def generate_quality_plot_per_source(self, query_frame: pd.DataFrame, plot_id:str=None, over_lod:bool=True, study_id:str=None) -> ResultObject:
        """
        Generates a quality control plot which depicts the number of (non-)NA values per metabolite.
        :param query_frame: pandas DataFrame containing the data to plot
        :param plot_id: The ID the plot is saved as, can be supploed (otherwise it is random)
        :param over_lod: If true, a >LOD plot will be drawn, otherwise <LOD.
        :returns BytesIO object containing the plot image
        """
        if query_frame.empty or not study_id:
            return ResultObject(msg="Plot creation failed!", status=400)
        
        # Remove study and phenotypes from input data, as they are not required here
        df = query_frame.drop(['study_id'], axis=1)
        df = df[df.columns.drop(list(df.filter(regex='PHENOTYPE_')))]
        f, ax = plt.subplots(figsize=(12, min(len(df), 400)))

        na_df = (df.isnull().sum(axis=1) / len(df.columns)) * 100
        if over_lod:
            missing_data = pd.DataFrame({'value': 100 - na_df})
        else:
            missing_data = pd.DataFrame({'value': na_df})

        _ = plt.yticks(ticks=range(0,len(missing_data)), labels=missing_data.index)
        barplot = sns.barplot(data=missing_data, x='value', y=missing_data.index, hue=missing_data.index,
                legend=False, orient='h', palette=sns.color_palette("hls", len(missing_data)))
        
        plt.title(f"Quality Control Plot for study {study_id}", loc='left', fontsize=19)
        plt.ylabel(f"Source", fontsize=15)
        plt.xlabel(f"Number of metabolites {'>' if over_lod else '<'} LOD (%)", fontsize=15) 
        _ = plt.xlim(0, 100)
        plt.tight_layout()

        bytes_image = io.BytesIO()
        FigureCanvas(f).print_png(bytes_image)
        #fig = barplot.get_figure()
        #fig.savefig("out_qc.png", dpi=400)
        plt.close('all')

        plot_id = self.save_plot(bytes_image, plot_id).getBody()
        return ResultObject(body=plot_id)


