class InvalidBiospecimenException(Exception):
    def __init__(self, erroneous_specimen):
        super().__init__(f"Biospecimen does not fit any known value: {erroneous_specimen}")
