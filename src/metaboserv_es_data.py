# Database index names
INDICES = {
    "STUDY": "study",
    "SOURCE": "source",
    "COMPOUND": "compound",
    "PHENOTYPE": "phenotype",
    "USER": "user",
    "TOKEN": "token",
    "UNIT": "units",
}

# Forbidden keywords
FORBIDDEN_IDS = ['compound', 'compound_map', 'hmdb_map', 'admin', 'admins', 'administrator', 'study', 'studies', 'source', 'sources', 'nconcs', 'nconc', 'create', 'insert', 'delete', 'metabolite', 'metabolites']

MAPPING_STUDY = {
    "properties": {
        "study_id": {"type": "keyword"},
        "study_type": {"type": "text"},
        "study_specimen": {"type": "text"},
        "study_name": {"type": "keyword"},
        "study_compounds": {"type": "text"},
        "study_visibility": {"type": "text"},
        "study_author": {"type": "text"},
        "study_metadata": {"type": "nested"},
        "study_members": {"type": "nested"},
        "study_date": {"type": "text"},
        "study_auth_tokens": {"type": "nested"},
    },
    
}

MAPPING_SOURCE = {
    "properties": {
        "source_id": {"type": "keyword"},
        "source_metadata": {"type": "nested"},
        "source_study": {"type": "keyword"},
    }
}

MAPPING_COMPOUND = {
    "properties": {
        "compound_id": {"type": "keyword"},
        "compound_inchikey": {"type": "keyword"},
        "compound_name": {"type": "text"},
        "compound_iupac": {"type": "text"},
        "compound_formula": {"type": "text"},
        "compound_mol_weight": {"type": "text"},
        "compound_synonyms": {"type": "text"},
        "taxonomy": {"type": "nested"},
        "hmdb_accession": {"type": "text"}
    }
}

MAPPING_UNIT = {
    "properties": {
        "study_id": {"type": "keyword"},
        "unit_map": {"type": "nested"},
    }
}

MAPPING_USER = {
    "properties": {
        "username": {"type": "keyword"},
        "password": {"type": "text"},
        "role": {"type": "text"},
        "salt": {"type": "text"},
        "studies": {"type": "nested"},
        "email": {"type": "text"},
        "institution": {"type": "text"},
        "verified": {"type": "text"},
    }
}

MAPPING_PHENOTYPE = {
    "properties": {
        "phenotype_data": {"type": "nested"},
        "phenotype_metadata": {"type": "nested"}, # metadata for a particulary study
        "phenotype_levels": {"type": "nested"},
        "phenotype_study": {"type": "keyword"}, # identifier of the according study
    }
}

MAPPING_TOKEN = {
    "properties": {
        "token": {"type": "keyword"},
        "creator": {"type": "text"},
        "date_creation": {"type": "text"},
        "date_expiration": {"type": "text"},
        "studies": {"type": "keyword"},
        "auth_level": {"type": "text"},
        "comment": {"type": "text"},
    }
}


MAPPINGS = {
    "COMPOUND": MAPPING_COMPOUND,
    "STUDY": MAPPING_STUDY,
    "USER": MAPPING_USER,
    "PHENOTYPE": MAPPING_PHENOTYPE,
    "TOKEN": MAPPING_TOKEN,
    "SOURCE": MAPPING_SOURCE,
    "UNIT": MAPPING_UNIT,
}
